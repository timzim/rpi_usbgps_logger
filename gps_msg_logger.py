#!/usr/bin/env python

import time, os

# The script tests BOTH of these constants!
# If either results in a TRUE test, the script exist
# Which ever constant is to be ignored, set it to 0
BYTES_TO_CAPTURE = 0
SECONDS_TO_CAPTURE = 3600*12 #12-hours

gpsmod = open("/dev/ttyACM0", "r")

t0 = t = time.time()
bytes = 0

# pi3 doesn't have an onboard RTC, so grab a timestamp from the GPS
# and open the file
print "Waiting for time from GPS module...",
while True:
    l = gpsmod.readline().split(',')
    if l[0] == "$GPRMC":
        print "[done]"
        fname = "./gpslog." + l[9] + "-" + l[1]
        gpslog = open(fname,"w")
        print "Initialized log file: " + fname
        break

while True:
  l = gpsmod.readline()
  if len(l) > 1:
    t = time.time()
    gpsstr = "{:.1f}".format(t-t0) + "," + l
    gpslog.write(gpsstr)
    bytes += len(gpsstr)
    if BYTES_TO_CAPTURE != 0:
      print("\rCalculating... %3d%%" % (100*bytes/BYTES_TO_CAPTURE)),
    else:
      print("\rCalculating... %3d%%" % ((100*(t-t0))/SECONDS_TO_CAPTURE) ),
  if (BYTES_TO_CAPTURE != 0   and bytes >= BYTES_TO_CAPTURE) or \
     (SECONDS_TO_CAPTURE != 0 and (t - t0) >= SECONDS_TO_CAPTURE):
    t = time.time()
    Bps  = bytes / (t - t0)
    Bph  = Bps * 3600
    MBph = Bph / (1024*1024)
    print("\nBytes: %d" % bytes)
    print("Seconds: %0.2f" % (t-t0))
    print("Rate: %0.2f Bps"   % Bps)
    print("Rate: %0.2f MB/hr" % MBph)
    break
gpsmod.close()
gpslog.close()

os.system("./shutdown.sh")

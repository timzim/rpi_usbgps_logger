#/usr/bin/env bash

# Turns on the GREEN and RED LED to let us know the script has finished
# and shutsdown the pi. MUST BE EXECUTED AS ROOT!

echo 100 > /sys/devices/platform/leds/leds/led0/brightness
echo 100 > /sys/devices/platform/leds/leds/led1/brightness

poweroff

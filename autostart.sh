#!/usr/bin/env bash

# Test for root user
if [ $(id -u) -ne "0" ]
then
  printf "\n[FAILURE] GPS Logger Autostart: You must be root.\n"
  exit 1
fi

# Disable the LED Trigger
echo none > /sys/class/leds/led0/trigger
echo none > /sys/class/leds/led1/trigger

# Change our working directory to the Project dir
cd /home/pi/Projects/gps_logger

# Start the process and save the PID
./gps_msg_logger.py > /dev/null & export GPSLOG_ID=$!

sleep 1.0

if ! ps -p $GPSLOG_ID > /dev/null
then
  >&2 echo -e "\n[FAILURE] Could not start the GPS Logger process...\n"
else
  # Flash the LED's to verify the scipt has been executed
  for i in {1..10}
  do
    echo 1 > /sys/class/leds/led0/brightness
    echo 1 > /sys/class/leds/led1/brightness
    sleep 0.5
    echo 0 > /sys/class/leds/led0/brightness
    echo 0 > /sys/class/leds/led1/brightness
    sleep 0.5
  done
fi

exit 0
